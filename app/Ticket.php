<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Ticket extends Model {
    // convention over configuration. table plural, model singular
    // by default/convention model ini mewakili table bernama tickets
    protected $table = 'ticket'; // tidak ikut convention. kena tambah configuration
    // table column primary ialah "id" dan "namatable_id" adalah foreign key
    public function comment() {
        return $this->hasMany(\App\Comment::class);
    }
}